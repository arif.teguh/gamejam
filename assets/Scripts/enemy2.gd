extends KinematicBody2D

export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)
var x = 0
var rightLeft = 1
var velocity = Vector2()
onready var animator = self.get_node("Animator")
onready var sprite = self.get_node("Sprite")


			
func _physics_process(delta):
	if(!global.zawarudo):
		velocity.y += delta * GRAVITY
		velocity.x = 100 * rightLeft
		if(x <= 100 ):
			x+=1
		if (x == 100):
			x = 0
			rightLeft = rightLeft * -1
		velocity = move_and_slide(velocity, UP)
func _process(delta):
	if velocity.x != 0:
		animator.play("Walk")
		if velocity.x > 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	if global.zawarudo:
		animator.play("Idle")