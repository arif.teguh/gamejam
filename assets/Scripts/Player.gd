extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -400
var time = 0
var maxtime = 100
var distance = 0
var double = true
const UP = Vector2(0,-1)
var velocity = Vector2()


func get_input():
	velocity.x = 0
	if Input.is_action_just_pressed('jump'):
		if is_on_floor() :
			velocity.y = jump_speed
			double = true
		elif !is_on_floor() and double==true: 
			velocity.y =  jump_speed
			double = false
		distance += 10
	if Input.is_action_pressed('right'):
		velocity.x += speed
		distance += 1
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		distance += 1
	if Input.is_action_pressed('down'):
		if (global.power >= 8) :
			global.zawarudo = true
			time  = maxtime
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	if(global.power <= 8) && (!global.zawarudo) && (distance >=100):
		distance = 0
		global.power +=  1
	if(global.zawarudo) && (time !=0):
		time -=1
		global.power = time /(maxtime/8)
	if(time == 0 ) && (global.zawarudo):
		global.zawarudo = false
		global.power = 0
		distance = 0
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if velocity.y != 0:
		$AnimatedSprite.play("Jump")
		if velocity.x > 0:
			$AnimatedSprite.flip_h = false
		else:
			$AnimatedSprite.flip_h = true
	elif velocity.x != 0:
		$AnimatedSprite.play("Walk")
		if velocity.x > 0:
			$AnimatedSprite.flip_h = false
		else:
			$AnimatedSprite.flip_h = true
	else:
		$AnimatedSprite.play("Idle")
	