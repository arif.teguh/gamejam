extends LinkButton

export(String) var scene_to_load

func _on_New_Game_pressed():
	global.lives = 3
	global.power = 0
	global.zawarudo = false
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
